import os
Base_Dir = os.getcwd()
Source_Dir = Base_Dir + '/Src'
exec(open(Base_Dir+'/Include/include.py').read())

app = Flask(__name__)


@app.route("/KPI_Correlations", methods=['GET'])
def main():  # subjectkpi_app_id,subjectkpi_layer,subjectkpi_comp_id,subjectkpi_metric_name,time_from,time_to

    app_id = request.args.get('app_id')
    subjectkpi_layer = request.args.get('subjectkpi_layer')
    time_from = request.args.get('time_from')
    time_to = request.args.get('time_to')
    callback=request.args.get('callback')
    #total_kpis=request.args.get('total_kpis')
    #DCK_kpis=request.args.get('DCK_kpis')
    #AD_KPI_Cor = AD_Correlation(app_id=str(app_id),
    #                            subjectkpi_layer=str(subjectkpi_layer),
    #                            time_from=str(time_from),
    #                            time_to=str(time_to),
    #                            roll_up_size=roll_up_size)
    AD_KPI_Cor = AD_RT_Correlation_Wrapper(str(app_id),str(time_from),str(time_to),str(subjectkpi_layer),total_kpis=6,DCK_kpis=4)

    AD_KPI_Cor_as_json = AD_KPI_Cor.to_dict(orient='records')
    r = Response(response=callback+"("+json.dumps(AD_KPI_Cor_as_json)+");", status=200)
    r.headers["Content-Type"] = "application/javascript"
    return r
    #return jsonify(AD_KPI_Cor_as_json)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)
