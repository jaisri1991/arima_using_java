def java_fun(data_set,app_name):
    percentage= 1 - (1/round(len(data_set)/roll_apply))
    freq=str(int(1440/roll_apply))+','+str(int(1440/roll_apply)*7)
    os.system('java -Xms5000m -jar jars\spark-arima-1.0-SNAPSHOT.jar -m SF -n '+str(percentage)+' -p Data\\'+app_name+'\Parameters.csv -sd 0.006,0.003 -t B -i '+freq+' -wl '+str(roll_apply)+' -d Data\\'+app_name+'\Raw_df.csv -a 1 -r')
    return percentage

def scoring_wrapper(comp_info,app_name,percentage):
    bl=pd.read_csv(Base_Dir+'/Data/'+app_name+'/Parameters.csv')
    ano_data=anomalydef_scoring(bl,comp_info,app_name,method,high_thr,kpi_threshold,txn_threshold,pcl,percentage,roll_apply,ema_win_len,high,med,low)
    ano_data.Time = ano_data.Time - datetime.timedelta(hours=5, minutes=30)
    table_update(ano_data)
    return ano_data

def table_update(df):
    t0=time.process_time()
    conn=dbfun()
    try:
        cursor=conn.cursor()
        cols = [col for col in df.columns if col not in ['Time', 'CompInstanceId']]
        data = df.set_index(['Time'])[cols].to_records().tolist()
        dbquery = """insert into AD_RT_ANOMALYDEF values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE  Time=VALUES(Time),App_Id=VALUES(App_Id),Comp_Id=VALUES(Comp_Id),Layer=VALUES(Layer),Win_Len=VALUES(Win_Len), Comp_Name=VALUES(Comp_Name), Metric_Name=VALUES(Metric_Name), Anomaly=VALUES(Anomaly),Mean=VALUES(Mean), Lower=VALUES(Lower),Upper=VALUES(Upper),Actual_Value=VALUES(Actual_Value),STL_Value=VALUES(STL_Value),AD_Method=VALUES(AD_Method),Original_Score=VALUES(Original_Score),Anomaly_Scored=VALUES(Anomaly_Scored);"""
        try:
            cursor.executemany(dbquery,data)
            conn.commit()
        except:
            conn.rollback()
        print("Time Taken For Mysql Update Is %s"%(time.process_time()-t0))
    except Exception as e:
        print(e)
    finally:
        cursor.close()