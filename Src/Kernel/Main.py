def main():
    # Source Files
    # Calling Functions
    pt0=time.process_time()
    print('Start Time For Main Function: ' + str(datetime.datetime.now()))
    app_list=pd.read_csv(Base_Dir + '\\config\\app_list.csv')
    ent_name='APPNOMIC'
    for app_name in list(app_list.App_Name):
        pt1 = time.process_time()
        pt2 = time.process_time()
        app_id,comp_list,txn_list=comp_txn(app_list,ent_name,app_name)
        last_refreshed = calcInputDateRange(app_id)
        frm = (pd.to_datetime(last_refreshed) - datetime.timedelta(days=30)).strftime('%Y-%m-%d %H:%M:%S')
        if(frm[14:16]!='00'):
            list1 = list(frm)
            list1[14:16] = ['0','0']
            frm=''.join(list1)
        if(last_refreshed==default_to):
            to = (pd.to_datetime(last_refreshed) + datetime.timedelta(minutes=60)).strftime('%Y-%m-%d %H:%M:%S')
        else:
            to = (pd.to_datetime(last_refreshed) + datetime.timedelta(minutes=119)).strftime('%Y-%m-%d %H:%M:%S')
        
        print('Fetching Data From %s and To %s'%(frm,to))
        data_set,comp_info=fetch_data(hostip,ent_name,app_id,frm,to,comp_list,txn_list)
        print("Dimension Of The Input Dataset: Rows: %d, Cols: %d and Min Time is %s and Max Time is %s"%(data_set.shape[0],data_set.shape[1],min(data_set.Time),max(data_set.Time)))
        print('Process Time To Fetch Data: ' + str(time.process_time()-pt2))
        pt2 = time.process_time()
        if(len(data_set)>0):
            data_set=impute_data(data_set,comp_info,app_name,app_id)        
            print('Process Time To Impute Data: ' + str(time.process_time()-pt2))
            
            #Java Arima
            pt2 = time.process_time()
            percentage=java_fun(data_set,app_name)
            print('Process Time For Java Arima: ' + str(time.process_time()-pt2))
            
            #Form DataFrame and Save the Results
            pt2 = time.process_time()
            ano_data=scoring_wrapper(comp_info,app_name,percentage)
            print('Process Time For Scoring and Mysql Update: ' + str(time.process_time()-pt2))
            print('Total Time for '+app_name+': ' + str(time.process_time()-pt1) +' Seconds And End Time: '+ str(datetime.datetime.now()))
    print('Total Process Time At The End Of Main Function: ' + str(time.process_time()-pt0))