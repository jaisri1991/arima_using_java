def stl_data(app_name):
    #os.chdir('java')
    files=os.listdir(Base_Dir+'/Data/'+app_name+'/STLOutput')
#    files=list(set(files) - set([name for name in files if 'KPI_' in name]))
#    files=list(set(files) - set([name for name in files if 'STL_' in name]))
#    files.sort()
    for file in files:
        kpi=file.replace('.csv','')
        #os.system('java -jar spark-arima-1.0-SNAPSHOT.jar -d ..\Data\\'+app_name+'\Indi_Kpis\\'+file+' -m S')
        sdata=pd.read_csv(Base_Dir+'/Data/'+app_name+'/STLOutput'+'/'+file)
        sdata=sdata[['Time',kpi]]
        sdata.Time = pd.to_datetime(sdata.Time,format='%d-%m-%Y %H:%M')
        if(file==files[0]):
            stl=sdata
        else:
            stl=stl.merge(sdata,on='Time',how='outer',sort=True)
    #os.chdir('../')
    stl.to_csv(Base_Dir+'/Data/'+app_name+'/STL_Preprocessed.csv',index=False)
    
def plot_java(app_name,percentage,win_len):

    files=os.listdir('Data/'+app_name+'/output/')
    ano_details=pd.DataFrame({'KPI':[0]*len(files),'Arima_Mean':0,'Nrow':0})
    j=0
    #df=non_overlapping_average(dd,win_len,'mean')
    if not os.path.exists('Plots/Java/'+app_name):
            os.makedirs('Plots/Java/'+app_name)
    os.chdir('Plots/Java/'+app_name)
    for file in files:
        kpi=file.replace('.csv','')
        data=pd.read_csv('Data/'+app_name+'/output/'+file)
        data.columns=data.columns.str.strip()
        col_name=data.filter(regex='3SD').columns[0]
        data.rename(columns={col_name:'Anomaly'}, inplace=True)
        c = [col for col in data.columns if col not in ['Time']]
        data[c]=data[c].apply(pd.to_numeric, errors='coerce')

        data.index=pd.to_datetime(data.Time,format='%d-%m-%Y %H:%M')
        axvl=data[round(len(data)*percentage):].index[0]
        plt.figure(figsize=(12,10))
        if(len(data.filter(regex='Actu').columns)==0):
            data.rename(columns={kpi:'Actual'}, inplace=True)        
        x = data['Actual']
        #y1 = data['Lower Interval']
        #y2 = data['Upper Interval']
        #y = data['Forecasts']
        plt.plot(x,color='orange')
        #plt.fill_between(x.index,y2,y1,interpolate=True,facecolor='green',alpha=0.2)
        #plt.plot(y,'--',color='b')
        high=data[(data['Anomaly']>0)].index
        plt.plot(data['Actual'][high],'ro',color='r')
        plt.axvline(x=axvl,color='r')
        plt.xticks(rotation=45)
        plt.title(str(kpi)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc')
        plt.savefig(str(kpi)+'_'+str(win_len)+'_Win_len and '+str(percentage)+'_Perc.jpg')
        ano_details.loc[j,'KPI']=kpi
        ano_details.loc[j,'Arima_Mean']=len(high)
        #ano_details.loc[j,'Nrow']=len(data[(data['Forecasts']>0)])
        j=j+1
    os.chdir('../../..')
    ano_details.to_csv('ano_details.csv')

def db_result(query):
    try:
        r = requests.get("http://%s:4567/sqlquery/%s" % (hostip,query))
        data=r.json()
        df=pd.DataFrame(data)
        if(len(df)>0):
            print('Total Row(s):', len(df))
        return df
    except Exception as e:
        print(e)

def comp_txn(app_list,ent_name,app_name):
    #app_list=pd.read_csv(Base_Dir+'/Data/app_list.csv')
    app=app_list[(app_list.App_Name==app_name)&(app_list.Ent_Name==ent_name)]
    comp_list=app.Comp_List.values[0]
    txn_list=app.Txn_List.values[0]
    app_id=app.App_Id.values[0]
    return app_id,comp_list,txn_list

def non_overlapping_average(raw_df,window_len,aggr_method='mean'):
    averaged_df=DataFrame()
    if((isinstance(raw_df.index,pd.core.indexes.datetimes.DatetimeIndex))):
        if (aggr_method == 'mean'):
            averaged_df = raw_df.resample(str(window_len)+'Min').mean()
        elif (aggr_method == 'median'):
            averaged_df = raw_df.resample(str(window_len)+'Min').median()
        elif (aggr_method == 'skew'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.skew)
        elif (aggr_method == 'kurt'):
            averaged_df = raw_df.resample(str(window_len)+'Min').apply(DataFrame.kurt)
    else:
        print('DataFrame Index is not DateTime')
    return averaged_df

def impute_using_sma(x,cols,n):
    #t1 = time.time()
    #print('Imputation Process Started')
    if(isinstance(cols,list)):
        for f in cols:
          #print(f)
          sma(x,f,n)
    else:
        sma(x,cols,n)
    #print("Total Time Taken For Imputation Process: %s" % (time.time() - t1))
    return x

def sma(x, f, n):
    count = len(x)
    ## Checks if the first element is NA if yes then replace with 0
    if (pd.isna(x.ix[0,f]) == True):
        nn = x[f].notnull().nonzero()[0]
        if (len(nn) > 1):
            x.ix[0, f]=x.ix[nn[0]:nn[1]+1, f].mean()
        else:
            x.ix[0, f] =0
    if (count > n):
        ### Method is SMA
        row_no = pd.isna(x[f]).nonzero()[0]
        if(len(row_no)>0):
            if((len(row_no)==count-1)&(x.ix[0, f]==0)):
                x[f]=x[f].fillna(0)
            else:
                ## For loop starts if the column has NaN
                for i in row_no:
                    if (i <= n):
                        x.ix[i, f] = x.ix[:i, f].mean()
                        ### if the values are greater than n
                    else:
                        x.ix[i, f] = x.ix[(i - n):(i - 1), f].mean()

def calcInputDateRange(app_id):
    sql_string = 'select max(Time) as Time from AD_RT_ANOMALYDEF where App_Id = ' + str(app_id)
    time_df=db_result(sql_string)
    if(time_df.Time[0]!=None):
        #Last refresh date
        last_refreshed = time_df['Time'].min().to_pydatetime()
        return last_refreshed
    else:
        return default_to
