def plot_raw_df(app_name):
    raw_df=pd.read_csv('Data/'+app_name+'/Raw_df.csv')
    disk_cols=list(raw_df.filter(regex='disk').columns)
    disk_data=raw_df[disk_cols]
    disk_data.index=pd.to_datetime(raw_df.Time,format='%Y-%m-%d %H:%M:%S')
    if not os.path.exists('Plots/'+app_name):
            os.makedirs('Plots/'+app_name)
    os.chdir('Plots/'+app_name)
    for d in disk_cols:
        x = disk_data[d]
        plt.figure(figsize=(12,10))
        plt.plot(x,color='orange')
        plt.xticks(rotation=45)
        plt.title(str(d))
        plt.savefig(str(d)+'.jpg')
    os.chdir('../..')
        