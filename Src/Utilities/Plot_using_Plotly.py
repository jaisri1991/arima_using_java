def plot_java_plotly(app_name):
    files=os.listdir('Data/'+app_name+'/output/')
    if not os.path.exists('Plots/Plotly/'+app_name):
            os.makedirs('Plots/Plotly/'+app_name)
    for file in files:
        kpi=file.replace('.csv','')
        data=pd.read_csv('Data/'+app_name+'/output/'+file)
        data.columns=data.columns.str.strip()
        col_name=data.filter(regex='3SD Ano').columns[0]
        data.rename(columns={col_name:'Anomaly'}, inplace=True)
        c = [col for col in data.columns if col not in ['Time']]
        data[c]=data[c].apply(pd.to_numeric, errors='coerce')
        data.index=pd.to_datetime(data.Time,format='%d-%m-%Y %H:%M')
        high = data[(data['Anomaly'] > 0)].index
        dd=[]
        t1=go.Scatter(
            x=data['Time'],
            y=data['STL processed'],
            mode='lines',
            name='Actual'
        )
        dd.append(t1)
        if 'Forecasts' in data.columns:
            t2 = go.Scatter(
                x=data['Time'],
                y=data['Forecasts'],
                mode='lines',
                name='Predicted'
            )
            dd.append(t2)
        t3 = go.Scatter(
            x=data['Time'][high],
            y=data['STL processed'][high],
            mode='markers',
            name='Actual',
            marker=dict(color='red')
        )
        dd.append(t3)
        layout = dict(title="Time Vs " + str(kpi))
        fig = dict(data=dd, layout=layout)
        plot(fig, filename='Plots/Plotly/'+app_name+'/'+kpi+'.html')