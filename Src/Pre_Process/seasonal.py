import pandas as pd
from seasonal import fit_seasons, adjust_seasons
import time,datetime

def seasonal(app_name,percentage=0.5):
    t0 = time.process_time()
    df = pd.read_csv('Data/RIB/Raw_df_Hour.csv')
    df.Time=pd.to_datetime(df.Time)
    df.index = df.Time
    cols = [col for col in df.columns if col not in ['Time']]
    f = open('Logs/Main.log', 'w')
    f1 = open('Logs/Season1.log', 'w')
    f2 = open('Logs/Season2.log', 'w')
    print('Start Time : ' + str(datetime.datetime.now()), file=f)
    NoneType = type(None)
    for col in cols[35:]:
        print(col)
        t1 = time.process_time()
        s = df[col]
        train=s[:int(len(s)*percentage)]
        test = s[int(len(s) * percentage):]
        s1=0
        s2=0
        for i in range(0,(len(test)+1)):
            data = s[i:(len(train) + i)]
            seasons, trend = fit_seasons(data)
            if (type(seasons) == NoneType):
                adjusted1 = data
                if(s1==0):
                    print(col,file=f1)
                    s1=1
            else:
                adjusted1 = adjust_seasons(data, seasons=seasons)
            seasons, trend = fit_seasons(adjusted1)
            if (type(seasons) == NoneType):
                adjusted = data
                if(s2==0):
                    print(col,file=f2)
                    s2=1
            else:
                adjusted = adjust_seasons(adjusted1, seasons=seasons)
            residual = adjusted - trend
            if(i==0):
                final=residual
            else:
                final=final.append(pd.Series(residual[len(residual)-1]))
        out=pd.DataFrame({'Time':s.index,col:final})
        if(col==cols[35]):
            result=out
        else:
            result=result.merge(out,on='Time',how='outer',sort=True)
        out.to_csv('Data/' + app_name + '/Seasonal_Output/' + col + '.csv', index=False)
        print('Total Time for ' + col + ' ' + str(time.process_time() - t1), file=f)
    f1.close()
    f2.close()
    print('Total Time Taken: ' + str(time.process_time() - t0), file=f)
    print('End Time : ' + str(datetime.datetime.now()), file=f)
    f.close()
    result.to_csv('Data/' + app_name + '/Seasonal_Output.csv', index=False)
    return result
