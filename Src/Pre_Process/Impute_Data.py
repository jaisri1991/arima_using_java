def impute_data(data_set,comp_info,app_name,app_id):
    ind=DataFrame({'Time':pd.date_range(min(data_set.Time),max(data_set.Time),freq='1min')})
    data_set=ind.merge(data_set,on='Time',how='outer',sort=True)
    bl = pd.read_csv(Base_Dir + '/Data/' + app_name + '/Parameters.csv')
    #bl = db_result('select * from AD_RT_ARIMACONFIG where AppInstanceId=' + str(app_id) + ' and Win_Len=' + str(roll_apply))
    bl_cols=bl.Metric_Name.tolist()
    bl_cols.append('Time')
    omit_list=set(data_set.columns)-set(bl_cols)
    data_set.drop(omit_list, axis=1, inplace=True)
    data_set=rule_wrapper(data_set)
    #data_set=data_set.fillna(method='ffill')
    #data_set=data_set.fillna(method='bfill')
    data_set=data_set.fillna(0)
    #imputated_data=impute_using_sma(data_set,cols,ema_win_len)
    if not os.path.exists(Base_Dir+'/Data/'+app_name):
        os.makedirs(Base_Dir+'/Data/'+app_name)
    #else:
    #    shutil.rmtree(Base_Dir+'/Data/'+app_name)
    #    os.makedirs(Base_Dir+'/Data/'+app_name)
    data_set.to_csv(Base_Dir+'/Data/'+app_name+'/Raw_df.csv',index=False)
    comp_info.to_csv(Base_Dir+'/Data/'+app_name+'/Comp_Info.csv',index=False)
    #bl.to_csv(Base_Dir+'/Data/'+app_name+'/Parameters.csv',index=False)
    return data_set

def rule1(data):
    x=data[:]
    na_count=x.isnull().sum()
    na_count = na_count / len(x)
    omit_list = na_count[na_count >= 0.3].index.tolist()
    x.drop(omit_list, axis=1, inplace=True)
    return x

def rule2(data):
    y=data[:]
    na_count = y.isnull().sum()
    na_count = na_count / len(y)
    cols = na_count[na_count >= 0.1].index.tolist()
    perc=int(len(y)*0.1)
    omit_list=[]
    for kpi in cols:
        for i in range(0,(len(y)-perc)):
            dd = y[i:(perc  + i)][kpi]
            na_count = dd.isna().sum()
            if (na_count >= perc):
                omit_list.append(kpi)
                break
    if (len(omit_list) > 0):
        y.drop(omit_list, axis=1, inplace=True)
    return y

def rule_wrapper(data):
    r1=rule1(data)
    r2=rule2(r1)
    return r2