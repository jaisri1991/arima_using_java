def df_anomalydef(bl,comp_info,app_name,percentage,roll_apply):
    files=os.listdir(Base_Dir+'/Data/'+app_name+'/output')
    peak_data=DataFrame()
    for file in files:
        kpi=file.replace('.csv','')
        #print(kpi)
        data=pd.read_csv(Base_Dir+'/Data/'+app_name+'/output'+'/'+file)
        data.Time = pd.to_datetime(data.Time, format='%d-%m-%Y %H:%M')
        dd=data[round(len(data)*percentage):]
        #dd.index=range(0,len(dd))
        kpi_d=bl[bl.Metric_Name==kpi]
        if(len(kpi_d)>0):
            kpi_d['Layer']=comp_info[comp_info.CompId==kpi_d.CompInstanceId.values[0]].Layer.values[0]
            kpi_d['CompName']=comp_info[comp_info.CompId==kpi_d.CompInstanceId.values[0]].Name.values[0]
            if(kpi_d['Layer'].values[0] != 'TXN'):
                kpi=kpi.replace(kpi_d.CompName.values[0]+'_','')
            df=DataFrame({'Time':dd.Time,'App_Id':[kpi_d.AppInstanceId.values[0]]*len(dd),'Comp_Id':[kpi_d.CompInstanceId.values[0]]*len(dd),'Layer':[kpi_d.Layer.values[0]]*len(dd),'Win_Len':[roll_apply]*len(dd),'Comp_Name':[kpi_d.CompName.values[0]]*len(dd),'Metric_Name':[kpi]*len(dd),'Anomaly':dd['3SD Anomalies'],'Mean':dd['Forecasts'],'Lower':dd['3SD Lower Interval'],'Upper':dd['3SD Upper Interval'],'Actual_Value':dd['Actual'],'AD_Method':['M001']*len(dd),'STL_Value':dd['STL processed']})
            peak_data=peak_data.append(df)
    peak_data=peak_data[['Time', 'App_Id', 'Comp_Id', 'Layer', 'Win_Len', 'Comp_Name', 'Metric_Name', 'Anomaly', 'Mean','Lower', 'Upper','Actual_Value','STL_Value','AD_Method']]
    return peak_data