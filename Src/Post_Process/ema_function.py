def ema(peak_data,frm,to,win_len):
        try:
            frm_index = peak_data[peak_data.Time==frm].index.values[0]
        except:
            frm=peak_data.Time[0]
            frm_index = peak_data[peak_data.Time == frm].index.values[0]
        try:
            to_index = peak_data[peak_data.Time == to].index.values[0]
        except:
            to=peak_data.Time[len(peak_data)-1]
            to_index = peak_data[peak_data.Time == to].index.values[0]
        dd=peak_data[:]
        if(frm_index > win_len):
            frm = peak_data.Time[frm_index-(win_len-1)]
            dd = peak_data[(peak_data.Time >= frm) & (peak_data.Time <= to)]
        if(to_index < len(dd)):
            dd = peak_data[(peak_data.Time >= frm) & (peak_data.Time <= to)]
        cols = [col for col in dd.columns if col not in ['Time']]
        ema_data = dd[cols].ewm(span = win_len,adjust = False).mean()
        ema_data.index = dd.Time
        return ema_data